let qtdCategoryChilds = 6;
let qtdProducts = 5;
let qtdSku = 2;
let vtexCategories = [];
let vtexChildCategories = []
let myCategory = [{
        "Name": "Eletrônicos",
        "Keywords": "Eletronicos",
        "Title": "eletronicos",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Celulares",
        "Keywords": "Celulares",
        "Title": "Celulares",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Decorações",
        "Keywords": "Decorações",
        "Title": "Decorações",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Ferramentas",
        "Keywords": "Ferramentas",
        "Title": "Ferramentas",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Acessórios",
        "Keywords": "Acessórios",
        "Title": "Acessórios",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Eletrodomesticos",
        "Keywords": "Eletrodomesticos",
        "Title": "Eletrodomesticos",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Camisas",
        "Keywords": "Camisas",
        "Title": "Camisas",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Calças",
        "Keywords": "Calças",
        "Title": "Calças",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Moletons",
        "Keywords": "Moletons",
        "Title": "Moletons",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    },
    {
        "Name": "Bones",
        "Keywords": "Bones",
        "Title": "Bones",
        "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
        "AdWordsRemarketingCode": null,
        "LomadeeCampaignCode": null,
        "FatherCategoryId": null,
        "GlobalCategoryId": 222,
        "ShowInStoreFront": true,
        "IsActive": true,
        "ActiveStoreFrontLink": true,
        "ShowBrandFilter": true,
        "Score": null,
        "StockKeepingUnitSelectionMode": "SPECIFICATION"
    }
];
let vtexTotalProducts = 56

function loadVtexCategory() {
    let xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === this.DONE) {
            let catResponse = JSON.parse(this.responseText)
            vtexCategories = catResponse
            subCategory()
        }
    });
    xhr.open("GET", "/api/catalog_system/pub/category/tree/1");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("accept", "application/json");

    xhr.send(null);
}

function loadVtexSubCategory(callback) {
    let xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === this.DONE) {
            let response = JSON.parse(this.responseText)
            for (let i = 0; i < response.length; i++) {
                for (let j = 0; j < response[i].children.length; j++) {
                    vtexChildCategories.push(response[i].children[j])
                }
            }
        }
        return callback()
    });
    xhr.open("GET", "/api/catalog_system/pub/category/tree/2");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("accept", "application/json");

    xhr.send(null);
}

function createSku(idProduct, callback) {
    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === this.DONE) {
            return callback()
        } else {
            console.log(this.readyState)
        }
    });

    xhr.open("POST", "/api/catalog/pvt/stockkeepingunit");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("accept", "application/vnd.vtex.ds.v10+json");

    xhr.send(JSON.stringify({
        "ProductId": idProduct,
        "IsActive": true,
        "Name": "sku teste" + idProduct,
        "RefId": "000" + idProduct,
        "PackagedHeight": 10,
        "PackagedLength": 10,
        "PackagedWidth": 10,
        "PackagedWeightKg": 10,
        "Height": 1,
        "Length": 1,
        "Width": 1,
        "WeightKg": 1,
        "CubicWeight": 1,
        "IsKit": false,
        "RewardValue": 1,
        "CommercialConditionId": 1,
        "MeasurementUnit": "un",
        "UnitMultiplier": 1,
        "KitItensSellApart": false
    }));
}

function createSkuStock(idSku, callback) {


    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function() {
        return callback()
    });

    xhr.open("PUT", "/api/logistics/pvt/inventory/skus/" + idSku + "/warehouses/1_1");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("accept", "application/vnd.vtex.ds.v10+json")
    xhr.send(JSON.stringify({
        "unlimitedQuantity": false,
        "dateUtcOnBalanceSystem": null,
        "quantity": 500
    }))
}

function createSkuImage(idSku, callback) {


    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === this.DONE) {
            return callback()
        }
    });

    xhr.open("POST", "/api/catalog/pvt/stockkeepingunit/" + idSku + "/file");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("accept", "application/vnd.vtex.ds.v10+json");

    xhr.send(JSON.stringify({
        "IsMain": true,
        "Label": "",
        "Name": "Royal-Canin-Feline-Urinary-SO",
        "Text": null,
        "Url": "https://tezmvn.vteximg.com.br/arquivos/produto-teste.jpg"
    }));
}

function createCategory(categoria, callback) {
    let xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === this.DONE) {
            return callback()
        } else {
            console.log(JSON.parse(xhr))
            return callback()
        }
    });
    xhr.open("POST", "/api/catalog/pvt/category");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("accept", "application/vnd.vtex.ds.v10+json");
    xhr.send(JSON.stringify(categoria));
}

function createProduct(child, callback) {
    let qtd = qtdProducts

    function next() {
        if (qtd) {
            var xhr = new XMLHttpRequest();

            xhr.addEventListener("readystatechange", function() {
                if (this.readyState === this.DONE) {
                    console.log(this.responseText);
                    qtd = qtd - 1
                    next()
                }
            });

            xhr.open("POST", "/api/catalog/pvt/product");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.setRequestHeader("accept", "application/vnd.vtex.ds.v10+json");

            xhr.send(JSON.stringify({
                "Name": "Produto" + qtd + " | " + child.name.split('|')[1] || child.id,
                "DepartmentId": child.id,
                "CategoryId": child.id,
                "BrandId": "2000000",
                "LinkId": "Produto" + qtd + " | " + child.name.split('|')[1] || child.name,
                "IsVisible": true,
                "Description": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium libero voluptas quod quae sunt mollitia debitis in nihil exercitationem at. Consectetur maiores hic culpa excepturi delectus ipsa autem, iste aliquid, praesentium unde quam, fuga neque cum aliquam explicabo. Dolorum accusamus error omnis ex ipsa odit a ea magnam ipsam? Maiores.",
                "DescriptionShort": "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi exercitationem consectetur inventore quisquam deserunt quae expedita pariatur eveniet repellat aliquid.",
                "KeyWords": "Produto" + qtd + " | " + child.name.split('|')[1] || child.name,
                "Title": "Produto" + qtd + " | " + child.name.split('|')[1] || child.name,
                "IsActive": true,
                "MetaTagDescription": "Produto" + qtd + " | " + child.name.split('|')[1] || child.name,
                "SupplierId": 1,
                "ShowWithoutStock": true,
                "Score": 1
            }));
        } else {
            console.log('cadastrei 3')
            return
        }
    }

    next()

    return callback()
}

function sku() {
    vtexTotalProducts = vtexTotalProducts - 1
    if (vtexTotalProducts > 0) {
        createSku(vtexTotalProducts, sku)
    } else {
        console.log('acabou')
    }

}

function subCategory() {
    let data = vtexCategories;
    let index = 5;
    if (data.length !== 0) {
        let currentData = data.pop()
        function subCategoryCreate(){
            console.log(currentData)
            index = index - 1;
            if(index){
                createCategory({
                    "Name": "FILHO" + index + " | " + currentData.name,
                    "Keywords": "Sub categoria",
                    "Title": "Sub categoria",
                    "Description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi laborum sapiente voluptatem? Consequatur officia quia odit, architecto, aspernatur dicta voluptas corporis illo aliquam obcaecati amet?",
                    "AdWordsRemarketingCode": null,
                    "LomadeeCampaignCode": null,
                    "FatherCategoryId": currentData.id,
                    "GlobalCategoryId": 222,
                    "ShowInStoreFront": true,
                    "IsActive": true,
                    "ActiveStoreFrontLink": true,
                    "ShowBrandFilter": true,
                    "Score": null,
                    "StockKeepingUnitSelectionMode": "SPECIFICATION"
                },subCategoryCreate);
            }else{
                return
            }
        }
        subCategoryCreate();
        subCategory();
    } else {
        return
    }
}

function category() {
    const categoria = myCategory.pop()
    if (categoria) {
        createCategory(categoria, category)
    } else {
        console.log('Terminou')
    }
}

function product() {
    const currentChild = vtexChildCategories.pop();
    if (currentChild) {
        createProduct(currentChild, product)
    } else {
        return
    }
}

function skuImage() {
    vtexTotalProducts = vtexTotalProducts - 1
    if (vtexTotalProducts > 0) {
        createSkuImage(vtexTotalProducts, skuImage)
    } else {
        console.log('acabo')
    }

}

function skuPrice() {
    vtexTotalProducts = vtexTotalProducts - 1
    if (vtexTotalProducts > 0) {
        createSkuImage(vtexTotalProducts, skuImage)
    } else {
        console.log('acabo')
    }

}

function skuStock() {
    vtexTotalProducts = vtexTotalProducts - 1
    if (vtexTotalProducts > 0) {
        createSkuStock(vtexTotalProducts, skuStock)
    } else {
        console.log('acabo')
    }

}


//category(); /* CADASTRA CATEGORIAS */
//loadVtexCategory(); /* CADASTRA SUB CATEGORIAS */
//loadVtexSubCategory(product); /* CADASTRA PRODUTOS */
//sku(); /* CADASTRA SKUS */
/* RECOMENDO SUBIR OS PREÇOS POR PLANILHA */
//skuStock(); /* CADASTRA ESTOQUE DO SKU */
//skuImage(); /* CADASTRA IMAGENS DO SKU */

//SUBIR IMAGENS POR ULTIMO PARA REIDEXAR O PRODUTO E PEGAR VALORES ATUALIZADOS DE ESTOQUE E PREÇO