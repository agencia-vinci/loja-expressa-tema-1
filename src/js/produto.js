//THIS FUNCTIONS LOAD OWL IN ALL VITRINES IN HOME PAGE AND DO VALIDATIONS
$('ul.thumbs').each(function(vitrine) {
    //VALIDATION TO CHECK IF EXISTS ITEMS ARE IN VITRINE
    const child = $(this).find('li')
    if (child.length <= 4) {

        return
    }
    //start vitrine
    $(this).owlCarousel({
        items: 4,
        autoPlay: true,
        autoHeight: true,
        pagination: false,
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [991, 3],
        itemsTablet: [767, 2],
        itemsMobile: [567, 1],
        navigation: true,
        stopOnHover: true,
        autoHeight: true,
        navigationText: ['<img src="/arquivos/seta-banner-esquerda.png" class="img-responsive arrow-vitrine" />', '<img src="/arquivos/seta-banner-direita.png" class="img-responsive arrow-vitrine" />'],
    });
})


setTimeout(function() {
    $('.product-rating strong').text('Avaliações: ')
    $('input#txtCep').ready(function() {
        $(this).attr('placeholder', 'Digite seu CEP')
    })

    const field = document.querySelector('.bt.freight-btn')
    field.setAttribute('value', 'Calcular')
}, 5000)




$(document).ready(function() {
    $('.bread-crumb ul li').first().html('<a title="TEMA 1" href="/">Página Inicial</a>');
});


$(document).ready(function() {
    $("ul.item-descrip li").click(function(e) {
        e.preventDefault();
        const element = $(this).children("a").attr('href');
        $('ul.item-descrip li').removeClass('item-desc-active');
        $(this).addClass('item-desc-active');
        $('.product-desc-container > div').hide();
        $(element).fadeIn();
    });
});


//THIS FUNCTIONS LOAD OWL IN ALL VITRINES IN HOME PAGE AND DO VALIDATIONS
$('.js-vitrine').each(function(vitrine) {
    //VALIDATION TO CHECK IF EXISTS ITEMS ARE IN VITRINE
    const child = $(this).find('.vitrine ul')

    if (!child) {
        $(child).remove()
        return
    }
    //take text to title
    $(this).find('.title-vitrine p').text($(this).find('.vitrine h2').text())
    $(this).find('.vitrine h2').remove()

    //start vitrine
    $(this).find('.vitrine').owlCarousel({
        items: 4,
        autoPlay: true,
        autoHeight: true,
        pagination: false,
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [991, 3],
        itemsTablet: [767, 2],
        itemsMobile: [567, 1],
        navigation: true,
        stopOnHover: true,
        autoHeight: true,
        navigationText: ['<img src="/arquivos/seta-banner-esquerda.png" class="img-responsive arrow-vitrine" />', '<img src="/arquivos/seta-banner-direita.png" class="img-responsive arrow-vitrine" />'],
    });
})